---
Install Gatsby
Init the project
---

# Gatsby + Sass

```
npm install -g gatsby-cli
```

```
gatsby new [name-application]
```

```
gatsby develop
```

Install package of Sass

```
npm install --save node-sass gatsby-plugin-sass
```

Include the plugin in your gatsby-config.js file.

gatsby-config.js

```js
plugins: [`gatsby-plugin-sass`]
```

I will put my index file ...

src/styles/index.scss

I will import
gatsby-browser.js

import "./src/index.scss"

npm install --save gatsby-plugin-catch-links
