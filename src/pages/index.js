import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"

import SEO from "../components/seo"

const IndexPage = ({ data }) => (
  <Layout>
    <SEO
      title="Home"
      meta={[
        {
          name: "keywords",
          constent: "gatsby, sass, react",
        },
      ]}
    />

    <div className="showcase">
      <h1>Enjoy the life!!!</h1>
    </div>

    <div className="container">
      <div className="main">
        {data.allMarkdownRemark.edges.map(post => (
          <div className="post-card my-2" key={post.node.id}>
            <div className="post-card__content p-1">
              <div className="content-title">
                <Link to={post.node.frontmatter.path}>
                  <h3>{post.node.frontmatter.title}</h3>
                </Link>
              </div>

              <div className="content-info">
                Post by {post.node.frontmatter.author} on{" "}
                {post.node.frontmatter.date}
              </div>

              <div className="content-description">{post.node.excerpt}</div>

              <Link className="btn-primary" to={post.node.frontmatter.path}>
                Read More
              </Link>
            </div>
          </div>
        ))}
      </div>

      <div className="aside">
        <div className="single-widget my-2">
          <h4 className="widget-title">Catagories</h4>
          <ul className="post-cat-list">
            <li>
              <a href="#">
                Watch<span>(07)</span>
              </a>
            </li>
            <li>
              <a href="#">
                Furniture<span>(08)</span>
              </a>
            </li>
            <li>
              <a href="#">
                Blog<span>(02)</span>
              </a>
            </li>
            <li>
              <a href="#">
                Car<span>(15)</span>
              </a>
            </li>
            <li>
              <a href="#">
                Medi<span>(11)</span>
              </a>
            </li>
            <li>
              <a href="#">
                Ligh<span>(06)</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </Layout>
)

export const pageQuery = graphql`
  query MyQuery {
    allMarkdownRemark {
      edges {
        node {
          id
          frontmatter {
            title
            path
            date
            author
          }
          excerpt
        }
      }
    }
  }
`

export default IndexPage
